Kubernetes - Jenkins
- Instalar Jenkins en Kubernetes
- Configurar Kubernetes como cloud de Jenkins para el aprovisionamiento dinamico de agentes

Pipelines
    Declarativa
        ^^ VV
    Scripted
    
Pipeline - scripted
    Parametros
        RAMA:
            Lista:  main
                    desarrollo
    Etapa 1
        Compilar un proyecto < Echo


-----

Multibranch
    Permite usar un Jenkinsfile diferente en cada rama.
        Siempre queremos eso?
            - Desarrollo -> Integracion - pruebas
            - Release   - Pre-produccion - pruebas adicionales
            - Main      - Produccion
    
        
    Main            <<< Produccion
        Release     <<< Prepproduccion (Pruebas de carga / Estres / HA)
            Desarrollo <<< Integración (Pruebas funcionales)
                Caractaristica 1 / Pruebas (Integracion)
                Caracteristica 2 / Pruebas (Integracion)
                Caracteristica 3 / Pruebas (Integracion)
                
GIT LAB (Apps)        
    Main            <<< Produccion
        Release     <<< Prepproduccion (Pruebas de carga / Estres / HA)
            Desarrollo <<< Integración (Pruebas funcionales)
                Caractaristica 1 / Pruebas (Integracion)
                Caracteristica 2 / Pruebas (Integracion)
                Caracteristica 3 / Pruebas (Integracion)

GITLAB (Jenkisfile) - vuestro
GITLAB (Jenkisfile) - desarrollo

            

git add * < Añadiros archivos 
                - de la carpeta actual
                - y cualquier subcarpeta
                NO OCULTOS
                
                
git add * < Añadiros archivos 
                - de la carpeta actual
                - y cualquier subcarpeta
                OCULTOS Y NO OCULTOS

git add :/ < Añadiros archivos 
                - de cualquier carpeta del proyecto
                OCULTOS Y NO OCULTOS
                
                
----------------
Jenkinsfile -> GIT
    Ejecutar:
        Carga la configuracion del JOB y lo ejecutas



REPO GIT PROYECTO
    Codigo proyecto

REPO GIT Jenkinsfiles
    Jenkinsfile v2 < triggerSCM 
    Si es un evento SCM el que dispara la ejecucion, solo carga propiedades, no ejecuta


configuracion Jenkins Configuracion v1