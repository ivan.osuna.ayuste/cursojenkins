Problemas potenciales con nuestro JOB ANSIBLE
- Parametrizar el INVENTARIO.
- De que dependemos? Necesitamos ansible en el nodo... Si no falla.
    - Realmente no controlo que instalacion/versión de ansible estoy usando.
        - ¿Cuál estamos utilizando? el que esté en el PATH
- Esto se ejecuta en el nodo maestro.

SOLUCIONES:
- Crear un nodo independiente de forma ESTATICA que tenga ansible instalado - En Docker Swarm
- Forzar la instalación de Ansible al ejecutar el job <<< Seguimos en el nodo maestro
- Contenedor de docker
- POD en kubernetes


PROYECTOS FREESTYLE . PROBLEMAS:
- Donde se esta guardando la configuración del proyecto: JENKINS... Si perdemos JENKINS... ups!
- Donde se quedan almacenados los CAMBIOS que vamos haciendo en el PROYECTO? En ningun sitio
    Solo se guarda la última versión del PROYECTO
- Estamos atados a tener 1 solo bloque de tareas
 



PROYECTO 1                    PROYECTO 2
TAREAS -> POST-TAREAS,        TAREAS -> POST-TAREAS      ? Como montarlo con proyectos de estilo libre
    - Enlazando PROYECTOS. No está mal...   que nos aporta flexibilidad... es un poco caos
    - ÑAPA GIGANTE: Instalar algunos plugins dentro de Jenkins: Flexible publish, AnyBuildStep
        Con eso podeis ejecutar posttareas en el bloque de las tareas y tareas en el bloque de las posttareas

Cuando queremos trabajas más serios con Jenkins, que tipo de proyectos montamos? Jenkinsfiles
    - La configuración del proyecto la guardamos en UN SCM
    - Donde se quedan almacenados los CAMBIOS que vamos haciendo en el PROYECTO? En nuestro SCM
    - Tengo mucha más potencia y flexibilidad a la hora de definir el JOB
    
----------------------------------------------------------------------
Jenkinsfiles
----------------------------------------------------------------------
Sintaxis? 
- Declarativa                   ----   pipeline
- Scripts   < Groovy            ----   node

Coinciden an algo? Algo se define igual ?
    Las tareas, ya a bajo nivel:
        - Llama a ansible
        - Ejecuta un script

ansiblePlaybook installation: 'MiAnsible', inventory: 'inventory/inventario.yaml', playbook: 'playbooks/playbook1.yaml'

Qué es lo que cambia?
    Es la manera de estructurar el fichero y de darle lógica
    
    Declarativa: Pipeline < etapas (stage) - agente (agent) - steps - tareas - post (casuistica)
        Van muy guiadas
    Script: Lo que definimos es un programa:
        Nodo (node) --- defino las tareas que quiero en ese nodo
            --- aqui no tengo post tarea, ni casuistica (success, fail, ....)
            --- lo que tengo es la potencia de un lenguaje de programacion: IF...
            
Esta forma de trabajar tiene un problema: Qué me cuesta definir?
    Las tareas, como llamar a los plugins   <<< Siempre es una buena práctica
    
    
Ansible
    Playbooks
        Tareas
            - Tipo >  name: install one specific version of Apache   *** SIEMPRE - Por qué?
                      yum:
                        name: httpd-2.2.29-1.4.amzn1
                        state: present
                
                     cmd: yum install httpd-2.2.29-1.4      
                     
                     
Los módulos en Ansible se desarrollan con el concepto de IDEMPOTENCIA?
Siempre el resultado es el mismo independientemente del estado inicial 


Sintaxis declarativa:

pipeline {
    
    // Nodo por defecto que va a ejecutar las tareas de todos los stages
    agent any
    
    // Estapas de nuestro pipeline
    stages {
    
        // Cada etapa del camino
        stage('Nombre de la etapa') {
            //agent {}
            
            // Son cada una de las tareas (pasos, builds)
            steps {
                ansiblePlaybook installation: 'MiAnsible', inventory: 'inventory/inventario.yaml', playbook: 'playbooks/playbook1.yaml'
            }
            
            // Las posttareas
            post {
                always {
                    
                }
                success {
                    
                }
            }
        }    
    }
    // Las posttareas
    post {
        always {
            
        }
        success {
            
        }
    }
}




PIPELINE:
    Etapa 1
        Mensaje consola
        posttareas:
            Mensaje consola: Siempre
    Etapa 2
        Mensaje Consola
    Etapa 3
        Mensaje consola  sh 'echo MENSAJE'
        Error    SH 'exit 1'
        Mensaje consola
        posttareas:
            Siempre : Mensaje
            Bien : Mensaje
            Mal : Mensaje
    posttareas:
        siempre: mensaje

----

Estructura a 1 nivel:

Pipeline:
    Etapas:
        Tareas

Pipeline:
    Etapas:
        Etapas: - En paralelo / secuencialmente
            Tareas
            
        Etapa:
            Matrix
matrix {
  axes {
    axis {
      name 'SISTEMA_OPERATIVO'
      values 'Linux','Macos','BSD'
    }axis {
      name 'PROGRAMAS'
      values 'apache','nginx'
    }
  }
  stages {
    stage('Hacer cosas') {
      steps {
        // One or more steps need to be included within the steps block.
      }
    }

  }

  excludes {
    exclude {
      axis {
        name 'PROGRAMAS'
        values 'apache'
      }
      axis {
        name 'SISTEMA_OPERATIVO'
        values 'BSD'
      }
    }
  }
}



QUEREMOS UN PARAMETRO LLAMADO EXPLOSION
0 1





node {
    checkout scm

    docker.withServer('tcp://172.31.13.34:2377') {
        docker.image('ubuntu').withRun('-p 3306:3306') {
            echo HOLA
        }
    }
}
